import java.io.*;

public class Many_Files {
    public static void main(String[] args) {
        try (
                BufferedReader bufferedReader = new BufferedReader(new FileReader("intdata.txt"));
                DataOutputStream outputStream = new DataOutputStream(new FileOutputStream("intdata.dat"))) {
            String string;

            while ((string = bufferedReader.readLine()) != null) {
                outputStream.writeInt(Integer.valueOf(string));
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
