import java.io.*;

public class Write_6numbers {
    public static void main(String[] args) {
        try (
                DataInputStream inputStream = new DataInputStream(new FileInputStream("intdata.dat"));
                DataOutputStream outputStream = new DataOutputStream(new FileOutputStream("int6data.dat"))) {
            int number;
            while (inputStream.available() > 0) {
                number = inputStream.readInt();
                if ((number < 1_000_000) && (number > 999)) {

                    outputStream.writeInt(number);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
