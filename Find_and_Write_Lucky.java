import java.io.*;
public class Find_and_Write_Lucky {
    public static void main(String[] args) {
        try (DataInputStream inputStream = new DataInputStream(new FileInputStream("int6data.dat"));
             BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("lucky.txt"))) {
            String string;
            while (inputStream.available() > 0) {
                string = correctForm(inputStream.readInt());
                if (isLucky(string)) {
                    bufferedWriter.write(string + "\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static boolean isLucky(String string) {
        char[] array = string.toCharArray();
        return array[0] + array[1] + array[2] == array[3] + array[4] + array[5];
    }

    private static String correctForm(int number) {
        StringBuilder string = new StringBuilder();
        int length = String.valueOf(number).length();
        for (int i = 0; i < 6 - length; i++) {
            string.append("0");
        }
        string.append(String.valueOf(number));
        return string.toString();
    }

    }

